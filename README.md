# AlgeSense

AlgeSense is an open source firmware for Arduino compatible microcontrollers to monitor alge with the help of RGB sensors. 

It's main target platform the Seeed WIO Terminal. It comes with a simple user-interface and is configured via a text file on a SDCard.
