#include "alge_sense_config.h"
#include "alge_sense_connection.h"
/* #include "alge_sense_rgb_data.h" */
#include "TFT_eSPI.h"
#include "alge_sense_rgb_sensor_tcs34725.h"
#include "alge_sense_ui.h"

#include "rpcWiFi.h" // WiFi library
//
#include "WiFiClientSecure.h" // TLS library
//
#include "PubSubClient.h" // MQTT library
// 320x240

TFT_eSPI tft;
WiFiClient client;
PubSubClient mqtt = PubSubClient(client);
AlgeSenseConnection conn = AlgeSenseConnection(&client, &mqtt);
AlgeSenseRgbSensorTCS34725 sensor = AlgeSenseRgbSensorTCS34725();
AlgeSenseConfig cfg;
/* AlgeSenseUI ui = AlgeSenseUI(TFT_HEIGHT, TFT_WIDTH, 5, &tft, &sensor, &conn);
 */
AlgeSenseUI *ui;
/* TFT_eSprite spr = ui.getChartSprite(); */
TFT_eSprite spr = TFT_eSprite(&tft);
String meta = "";

void setup() {
  tft.begin();
  tft.setRotation(3);
  tft.fillScreen(TFT_BLACK);
  tft.println("Initializing");
  Serial.begin(9600);
  tft.print("Initializing Sensor...");
  // initial read for the chart
  sensor.read();
  sensor.ledOff();
  tft.println("Done");

  cfg.load();

  if (cfg.wifiAvailable) {
    tft.println("Initializing WIFI connection to: " + cfg.wifiSSID);
    conn.connectWifi(cfg.wifiSSID.c_str(), cfg.wifiPassword.c_str());
    tft.println("Connected.");
    if (cfg.mqttAvailable) {
      tft.println("Initializing MQTT connection to" + cfg.mqttServer);
      conn.connectMqtt(cfg.mqttDeviceId.c_str(), cfg.mqttServer.c_str());
      tft.println("Connected.");
  mqtt.publish("foo", "bar");
    }
  } else {
    tft.println("Wifi not found");
  }
  if (cfg.metaAvailable) {
    meta = cfg.meta;
  }
  tft.print("Initializing UI...");
  sensor.read();
  ui = new AlgeSenseUI(TFT_HEIGHT, TFT_WIDTH, 5, &tft, &spr, &sensor, &conn);
  tft.println("Done");

  tft.print("Initializing Chart...");
  auto value = sensor.getValue();
  ui->updateData(value);
  tft.println("Done");
  tft.println("Initialized");
  /* delay(3000); */
  tft.fillScreen(TFT_BLACK);
}
int i = 0;

void loop() {
/* mqtt.loop(); */
  /* mqtt.publish("foo", "bar"); */
  if (ui->running) {
    sensor.read();
    auto value = sensor.getValue();
    ui->updateData(value);
    String data = rgbDataToJson(value, "");
    i++;
      Serial.println("Publish: " + String(i));
    if (conn.connected()) {
      Serial.println("Publish: " + data);

      /* conn.connectMqtt(cfg.mqttDeviceId.c_str(), cfg.mqttServer.c_str()); */
      /* conn.publish("muh"); */
    }
  } else {
    delay(50);
  }
  ui->loop();
}
