#pragma once
#include "seeed_line_chart.h"

#define max_size 30 // maximum size of data

class AlgeSenseChart {
public:
  AlgeSenseChart(double x, double y, double width, double height, TFT_eSprite *spr);
  void addData(double red, double green, double blue, double clear);
  void draw();

private:
  double _x;
  double _y;
  double _height;
  double _width;
  doubles _red;
  doubles _green;
  doubles _blue;
  doubles _clear;
  TFT_eSprite *_sprite;

  void drawChart(doubles data, int color);
};

AlgeSenseChart::AlgeSenseChart(double x, double y, double width,
                               double height, TFT_eSprite *spr) {
  _x = x;
  _y = y;
  _height = height;
  _width = width;
  spr->setRotation(3);
  spr->createSprite(_height, _width);
	_sprite = spr;
}

void AlgeSenseChart::addData(double red, double green, double blue,
                             double clear) {
  if (_red.size() == max_size) {
		Serial.println("pop");
    _red.pop();
    _green.pop();
    _blue.pop();
    _clear.pop();
  }
  _red.push(red);
  _green.push(green);
  _blue.push(blue);
  _clear.push(clear);
}

void AlgeSenseChart::draw() {
  _sprite->fillSprite(TFT_WHITE);
  drawChart(_red, TFT_RED);
	drawChart(_green, TFT_GREEN);
	drawChart(_blue, TFT_BLUE);
	drawChart(_clear, TFT_BLACK);
  _sprite->pushSprite(_x, _y);
}

void AlgeSenseChart::drawChart(doubles data, int color) {
  // Chart is rotated
  auto chart = line_chart(0, 0); //(x,y) where the line graph begins
  chart
      .height(_width)     // actual height of the line chart
      .width(_height)     // actual width of the line chart
      .based_on(0.0)      // Starting point of y-axis, must be a float
      .show_circle(false) // drawing a cirle at each point, default is on.
      .value(data)        // passing through the data to line graph
      .color(color)       // Setting the color for the line
      .draw();
}
