#pragma once
#include <SPI.h>
#include <Seeed_FS.h>
//
#include "SD/Seeed_SD.h"

class AlgeSenseConfig {
public:
  void load();
  bool wifiAvailable = false;
  String wifiSSID;
  String wifiPassword;
  bool mqttAvailable = false;
  String mqttDeviceId;
  String mqttServer;
  bool metaAvailable = false;
  String meta;

private:
  File cfgFile;
  char _readBuffer[255];
  bool readNextLine(String *line);
};

void AlgeSenseConfig::load() {
  if (!SD.begin(SDCARD_SS_PIN, SDCARD_SPI)) {
    return;
  }

  cfgFile = SD.open("alge_sense_config.txt", FILE_READ); // Read Mode
  if (cfgFile) {

    if (!readNextLine(&wifiSSID) || !readNextLine(&wifiPassword))
      return;

    wifiAvailable = true;

    if (!readNextLine(&mqttDeviceId) || !readNextLine(&mqttServer))
      return;

    mqttAvailable = true;

    if (!readNextLine(&meta))
      return;

    metaAvailable = true;

    // close the file:
    cfgFile.close();
  }
}

bool AlgeSenseConfig::readNextLine(String *line) {
  if (cfgFile.available()) {
    int index = 0;

    while (cfgFile.available()) {
      char nextChar = (char)cfgFile.read();
      if (nextChar == '\n') {
        _readBuffer[index] = '\0';
        *line = String(_readBuffer);
        return true;
      } else {
        _readBuffer[index] = nextChar;
        index += 1;
      }
    }
  }
  return false;
}
