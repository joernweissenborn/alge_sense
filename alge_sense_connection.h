#pragma once

#include "rpcWiFi.h" // WiFi library
//
#include "WiFiClientSecure.h" // TLS library
//
#include "PubSubClient.h" // MQTT library
class AlgeSenseConnection {
public:
  AlgeSenseConnection(WiFiClient *client, PubSubClient *mqtt);
  void connectWifi(const char *ssid, const char *password);
  void connectMqtt(const char *deviceId, const char *server);
  String getSSID();
  String getDeviceId();
  bool connected();
  void publish(String data);

private:
  String _ssid;
  String _deviceId;
  bool _connected = false;

  WiFiClient *_client;
  PubSubClient *_mqtt;
};

AlgeSenseConnection::AlgeSenseConnection(WiFiClient *client,
                                         PubSubClient *mqtt) {
  _client = client;
  _mqtt = mqtt;
}
bool AlgeSenseConnection::connected() { return _connected; }

String AlgeSenseConnection::getSSID() { return _ssid; }
String AlgeSenseConnection::getDeviceId() { return _deviceId; }

void AlgeSenseConnection::connectWifi(const char *ssid, const char *password) {
  WiFi.mode(WIFI_STA);
  WiFi.disconnect(true);

  _ssid = ssid;
  while (WiFi.status() != WL_CONNECTED) {
    WiFi.status();
    WiFi.begin(ssid, password);
    delay(500);
  }
}

void AlgeSenseConnection::connectMqtt(const char *deviceId,
                                      const char *server) {

  _mqtt->setServer(server, 1883);
  _mqtt->setKeepAlive(90);

  _deviceId = deviceId;
  while (!_mqtt->connected()) {
    if (_mqtt->connect(deviceId)) {
      _connected = true;
    } else {
      delay(1000);
    }
  }
}
void AlgeSenseConnection::publish(String data) {
  _mqtt->publish("AlgeSenseRgbData", data.c_str());
}
