#pragma once
#include "Adafruit_TCS34725.h"
#include "TFT_eSPI.h"
#include "alge_sense_rgb_sensor.h"

#define header_size 10
#define footer_size 40

class AlgeSenseButton {
public:
  AlgeSenseButton(int button);
  bool pressed();

private:
  int _button;
};

AlgeSenseButton::AlgeSenseButton(int button) {
  _button = button;
  pinMode(_button, INPUT_PULLUP);
}

bool AlgeSenseButton::pressed() {
  bool isPressed = false;
  while (digitalRead(_button) == LOW) {
    analogWrite(WIO_BUZZER, 128);
    isPressed = true;
    delay(50);
  }
  analogWrite(WIO_BUZZER, 0);
  delay(50);
  return isPressed;
}

class AlgeSenseControlElement {
public:
  AlgeSenseControlElement(double x, double y, double width, int index,
                          TFT_eSPI *tft, AlgeSenseRgbSensor *sensor);
  virtual void draw(bool active);
  virtual void onUp();
  virtual void onDown();

protected:
  double _x;
  double _y;
  double _width;
  int _index;
  void drawBox(bool active);
  void drawTitle(bool active, String title);
  void drawValue(bool active, String value);
  TFT_eSPI *_tft;
  AlgeSenseRgbSensor *_sensor;
};

AlgeSenseControlElement::AlgeSenseControlElement(double x, double y,
                                                 double width, int index,
                                                 TFT_eSPI *tft,
                                                 AlgeSenseRgbSensor *sensor) {
  _x = x;
  _y = y;
  _width = width;
  _index = index;
  _tft = tft;
  _sensor = sensor;
}

void AlgeSenseControlElement::drawBox(bool active) {
  if (active) {
    _tft->fillRect(_x + _index * _width, _y, _width, footer_size, TFT_WHITE);
  } else {
    _tft->drawRect(_x + _index * _width, _y, _width, footer_size, TFT_WHITE);
  }
}

void AlgeSenseControlElement::drawTitle(bool active, String title) {
  _tft->setTextColor(active ? TFT_BLACK : TFT_WHITE);
  _tft->drawString(title, _x + _index * _width + 2, _y + 2);
}

void AlgeSenseControlElement::drawValue(bool active, String value) {
  _tft->setTextColor(active ? TFT_BLACK : TFT_WHITE);
  _tft->drawString(value, _x + _index * _width + 2, _y + 12);
}

class AlgeSenseControlQuality : public AlgeSenseControlElement {
  using AlgeSenseControlElement::AlgeSenseControlElement;

public:
  void draw(bool active);
  void onUp();
  void onDown();
};

void AlgeSenseControlQuality::draw(bool active) {
  drawBox(active);
  drawTitle(active, "Quality");
  drawValue(active, String(_sensor->quality));
}
void AlgeSenseControlQuality::onUp() { _sensor->quality++; }
void AlgeSenseControlQuality::onDown() { _sensor->quality--; }

class AlgeSenseControlIntegration : public AlgeSenseControlElement {
  using AlgeSenseControlElement::AlgeSenseControlElement;

public:
  void draw(bool active);
  void onUp();
  void onDown();

private:
  int _currentIndex = 18;

  const String _integrationTimeStrings[19] = {
      "2_4MS", "24MS",  "50MS",  "60MS",  "101MS", "120MS", "154MS",
      "180MS", "199MS", "240MS", "300MS", "360MS", "401MS", "420MS",
      "480MS", "499MS", "540MS", "600MS", "614MS",
  };
};

void AlgeSenseControlIntegration::draw(bool active) {
  drawBox(active);
  drawTitle(active, "Integration");
  drawValue(active, String(_sensor->getIntegrationTime(_currentIndex)));
}

void AlgeSenseControlIntegration::onUp() {
  if (_currentIndex < _sensor->nrIntegrationTimes() - 1) {
    _currentIndex++;
    _sensor->setIntegrationTime(_currentIndex);
  }
}

void AlgeSenseControlIntegration::onDown() {
  if (_currentIndex > 0) {
    _currentIndex--;
    _sensor->setIntegrationTime(_currentIndex);
  }
}

class AlgeSenseControlGain : public AlgeSenseControlElement {
  using AlgeSenseControlElement::AlgeSenseControlElement;

public:
  void draw(bool active);
  void onUp();
  void onDown();

private:
  uint8_t _currentIndex = 0;
  const String _gainStrings[4] = {"1X", "4X", "16X", "60X"};
};

void AlgeSenseControlGain::draw(bool active) {
  drawBox(active);
  drawTitle(active, "Gain");
  drawValue(active, String(_sensor->getGain(_currentIndex)));
}

void AlgeSenseControlGain::onUp() {
  if (_currentIndex < _sensor->nrGains() - 1) {
    _currentIndex++;
    _sensor->setGain(_currentIndex);
  }
}

void AlgeSenseControlGain::onDown() {
  if (_currentIndex > 0) {
    _currentIndex--;
    _sensor->setGain(_currentIndex);
  }
}

class AlgeSenseControlLed : public AlgeSenseControlElement {
  using AlgeSenseControlElement::AlgeSenseControlElement;

public:
  void draw(bool active);
  void onUp();
  void onDown();

private:
  bool _on = false;
};

void AlgeSenseControlLed::draw(bool active) {
  drawBox(active);
  drawTitle(active, "LED");
  drawValue(active, _on ? "ON" : "OFF");
}

void AlgeSenseControlLed::onUp() {
  _on = !_on;
  if (_on) {
    _sensor->ledOn();
  } else {
    _sensor->ledOff();
  }
}

void AlgeSenseControlLed::onDown() { onUp(); }
