#pragma once

struct AlgeSenseRgbData {
  int red;
  int green;
  int blue;
  int clear;
  int quality;
  double integrationTime;
  int gain;
};

String rgbDataToJson(AlgeSenseRgbData value, String meta) {
  return "{\"red\": " + String(value.red) +
         ", \"green\": " + String(value.green) +
         ", \"blue\": " + String(value.blue) +
         ", \"clear\": " + String(value.clear) +
         ", \"quality\": " + String(value.quality) +
         ", \"integration_time\": " + String(value.integrationTime) +
         ", \"gain\": " + String(value.gain) + ", \"meta\": \"" + meta + "\"}";
}
