#pragma once

#include "alge_sense_rgb_data.h"

class AlgeSenseRgbSensor {
public:
  AlgeSenseRgbData getValue();
  virtual bool setup();
  virtual void ledOn();
  virtual void ledOff();
  virtual void read();
  virtual void setIntegrationTime(int index);
  virtual double getIntegrationTime(int index);
  virtual void setGain(int index);
  virtual int getGain(int index);
  virtual int nrIntegrationTimes();
  virtual int nrGains();
  int quality = 0;

protected:
  uint16_t red;
  uint16_t green;
  uint16_t blue;
  uint16_t clear;
  double integrationTime;
  int gain;
};

AlgeSenseRgbData AlgeSenseRgbSensor::getValue() {
  return {int(red), int(green),      int(blue), int(clear),
          quality,  integrationTime, gain};
}
