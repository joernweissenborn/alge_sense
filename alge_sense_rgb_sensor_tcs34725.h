#pragma once

#include <Wire.h>
//
#include "Adafruit_TCS34725.h"

#include "alge_sense_rgb_data.h"
#include "alge_sense_rgb_sensor.h"

class AlgeSenseRgbSensorTCS34725 : public AlgeSenseRgbSensor {
public:
  AlgeSenseRgbSensorTCS34725();
  bool setup();
  void ledOn();
  void ledOff();
  void read();
  void setIntegrationTime(int index);
  double getIntegrationTime(int index);
  void setGain(int index);
  int getGain(int index);
  int nrIntegrationTimes();
  int nrGains();

private:
  const uint8_t _integrationTimeConstants[19] = {
      TCS34725_INTEGRATIONTIME_2_4MS, TCS34725_INTEGRATIONTIME_24MS,
      TCS34725_INTEGRATIONTIME_50MS,  TCS34725_INTEGRATIONTIME_60MS,
      TCS34725_INTEGRATIONTIME_101MS, TCS34725_INTEGRATIONTIME_120MS,
      TCS34725_INTEGRATIONTIME_154MS, TCS34725_INTEGRATIONTIME_180MS,
      TCS34725_INTEGRATIONTIME_199MS, TCS34725_INTEGRATIONTIME_240MS,
      TCS34725_INTEGRATIONTIME_300MS, TCS34725_INTEGRATIONTIME_360MS,
      TCS34725_INTEGRATIONTIME_401MS, TCS34725_INTEGRATIONTIME_420MS,
      TCS34725_INTEGRATIONTIME_480MS, TCS34725_INTEGRATIONTIME_499MS,
      TCS34725_INTEGRATIONTIME_540MS, TCS34725_INTEGRATIONTIME_600MS,
      TCS34725_INTEGRATIONTIME_614MS,
  };
  const double _integrationTimes[19] = {
      2.4, 24,  50,  60,  101, 120, 154, 180, 199, 240,
      300, 360, 401, 420, 480, 499, 540, 600, 614,
  };
  const int _gains[4] = {1, 4, 16, 60};

  Adafruit_TCS34725 _tcs;
};

AlgeSenseRgbSensorTCS34725::AlgeSenseRgbSensorTCS34725() {
  gain = 1;
  integrationTime = 614;
  _tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_614MS, TCS34725_GAIN_1X);
}

bool AlgeSenseRgbSensorTCS34725::setup() { return _tcs.begin() == 1; }

void AlgeSenseRgbSensorTCS34725::read() {
  _tcs.getRawData(&red, &green, &blue, &clear);
}

void AlgeSenseRgbSensorTCS34725::ledOn() { _tcs.setInterrupt(false); }

void AlgeSenseRgbSensorTCS34725::ledOff() { _tcs.setInterrupt(true); }

void AlgeSenseRgbSensorTCS34725::setIntegrationTime(int index) {
  integrationTime = _integrationTimes[index];
  _tcs.setIntegrationTime(_integrationTimeConstants[index]);
}

double AlgeSenseRgbSensorTCS34725::getIntegrationTime(int index) {
  return _integrationTimes[index];
}

void AlgeSenseRgbSensorTCS34725::setGain(int index) {
  gain = _gains[index];
  _tcs.setGain((tcs34725Gain_t)index);
}

int AlgeSenseRgbSensorTCS34725::getGain(int index) { return _gains[index]; }

int AlgeSenseRgbSensorTCS34725::nrIntegrationTimes() { return 19; }
int AlgeSenseRgbSensorTCS34725::nrGains() { return 4; }
