#pragma once
#include "TFT_eSPI.h"
#include "alge_sense_chart.h"
#include "alge_sense_config.h"
#include "alge_sense_connection.h"
#include "alge_sense_control.h"
#include "alge_sense_rgb_data.h"
#include "alge_sense_rgb_sensor.h"
#include "alge_sense_rgb_sensor_tcs34725.h"

class AlgeSenseUI {
public:
  AlgeSenseUI(double width, double height, double margin, TFT_eSPI *tft,
              TFT_eSprite *spr, AlgeSenseRgbSensor *sensor,
              AlgeSenseConnection *conn);
  void readButtons();
  void draw();
  void updateData(AlgeSenseRgbData value);
  void loop();
  double width();
  int nrControls = 4;
  bool running = false;

private:
  double _height;
  double _width;
  double _margin;
  TFT_eSPI *_tft;
  AlgeSenseChart *_chart;
  AlgeSenseRgbSensor *_sensor;
  AlgeSenseConnection *_conn;
  AlgeSenseButton *_runButton = new AlgeSenseButton(WIO_KEY_C);
  AlgeSenseButton *_lButton = new AlgeSenseButton(WIO_5S_LEFT);
  AlgeSenseButton *_rButton = new AlgeSenseButton(WIO_5S_RIGHT);
  AlgeSenseButton *_uButton = new AlgeSenseButton(WIO_5S_UP);
  AlgeSenseButton *_dButton = new AlgeSenseButton(WIO_5S_DOWN);
  AlgeSenseControlElement *_controls[4];

  int _currentIndex = 0;

  void drawHeader();
  void drawFooter();
};

AlgeSenseUI::AlgeSenseUI(double width, double height, double margin,
                         TFT_eSPI *tft, TFT_eSprite *spr,
                         AlgeSenseRgbSensor *sensor,
                         AlgeSenseConnection *conn) {
  _height = height - 2 * margin;
  _width = width - 2 * margin;
  _margin = margin;
  _tft = tft;
  _sensor = sensor;
  _conn = conn;
  pinMode(WIO_BUZZER, OUTPUT);

  double chartHeight = _height - header_size - footer_size;
  _chart = new AlgeSenseChart(_margin, _margin + header_size, chartHeight,
                              _width, spr);
  double controlY = _margin + header_size + chartHeight;
  double controlWidth = _width / nrControls;
  _controls[0] = new AlgeSenseControlQuality(_margin, controlY, controlWidth, 0,
                                             tft, _sensor);
  _controls[1] = new AlgeSenseControlIntegration(_margin, controlY,
                                                 controlWidth, 1, tft, _sensor);
  _controls[2] = new AlgeSenseControlGain(_margin, controlY, controlWidth, 2,
                                          tft, _sensor);
  _controls[3] =
      new AlgeSenseControlLed(_margin, controlY, controlWidth, 3, tft, _sensor);
}

void AlgeSenseUI::loop() {
  readButtons();
  draw();
}

void AlgeSenseUI::readButtons() {
  if (_runButton->pressed()) {
    running = !running;
    _tft->fillScreen(TFT_BLACK);
  }
  if (running)
    return;
  if (_lButton->pressed()) {
    if (_currentIndex != 0) {
      _currentIndex--;
      _tft->fillScreen(TFT_BLACK);
    }
  }
  if (_rButton->pressed()) {
    if (_currentIndex != nrControls - 1) {
      _currentIndex++;
      _tft->fillScreen(TFT_BLACK);
    }
  }
  if (_uButton->pressed()) {
    _controls[_currentIndex]->onUp();
  }
  if (_dButton->pressed()) {
    _controls[_currentIndex]->onDown();
  }
}
void AlgeSenseUI::updateData(AlgeSenseRgbData value) {
  _chart->addData(value.red, value.green, value.blue, value.clear);
}

void AlgeSenseUI::draw() {
  drawHeader();
  drawFooter();
  _chart->draw();
}

void AlgeSenseUI::drawHeader() {
  _tft->setTextColor(TFT_WHITE);
  _tft->drawString(running ? "Running" : "Stopped", _margin, _margin);
  _tft->drawString(_conn->connected() ? _conn->getSSID() : "Offline",
                   _margin + _width - 75, _margin);
  if (_conn->connected()) {
    _tft->drawString(_conn->getDeviceId(), _margin + _width / 2, _margin);
  }
}

void AlgeSenseUI::drawFooter() {
  for (int i = 0; i < nrControls; i++) {
    _controls[i]->draw(!running && i == _currentIndex);
  }
}
